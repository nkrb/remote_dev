#!/bin/bash
# 
# Usage:
#    bash setup_ssh.sh username@ip
#    bash setup_ssh.sh username@hostname
#
#
# Notes:
#  If this doesnt work, make sure authorized_keys has permissions 644 and .ssh folder has permissions 700
#
#  If it still doesn't work:
#    quoting http://www.linuxproblem.org/art_9.html
#    "A note from one of our readers: Depending on your version of SSH you might also have to do the following changes:
#    Put the public key in .ssh/authorized_keys2
#    Change the permissions of .ssh to 700
#    Change the permissions of .ssh/authorized_keys2 to 640"

if ! [ -f ~/.ssh/id_rsa.pub ]; then
    ssh-keygen -t rsa
fi

ssh $1 mkdir -p .ssh
cat ~/.ssh/id_rsa.pub | ssh $1 'cat >> .ssh/authorized_keys'
echo 'Finished transferring SSH keys'