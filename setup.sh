#!/bin/bash

mkdir -p ~/bin/
cp bin/killtunnels ~/bin/; chmod +x ~/bin/killtunnels
cp bin/tunnel ~/bin/; chmod +x ~/bin/tunnel

cp bin/pull ~/bin/; chmod +x ~/bin/pull
cp bin/push ~/bin/; chmod +x ~/bin/push
cp bin/pulldiff ~/bin/; chmod +x ~/bin/pulldiff

cp bin/*.t ~/bin/

printf "\n" >> ~/.bashrc
echo "export PATH=$PATH:~/bin" >> ~/.bashrc