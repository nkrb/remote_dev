
Working remotely, efficiently
=============================

This reverse tunneling based `ssh` setup helps enable rapid remote development 1) efficiently local, and 2) possible with low-speed or high-jitter internet connections, eg. in tier 3 cities or villages beyond.

An ssh tunnel is established from local->remote after which pulling changes from local and testing them on remote are done together directly on a remote terminal as,
```
<remoteuser>@<remoteip>:~/project_dir$ pull; bash program.sh; push ./results/
```

Advantages:
1. Efficient local development (no terminal switching and waiting for transfers; extremely rapid iteration)
	- Simple `scp` or `rsync` workflow: 5 application switches/waits per iteration
		* editor (save) --> local terminal (`rsync` changes) --> wait for transfer to complete --> remote terminal (`bash program.sh`) --> wait for program to finish --> local terminal (`rsync` results)
	- Reverse `ssh` tunneling workflow: 1 application swtich/no waits per iteration
		* editor (save) --> remote terminal (`pull; bash program.sh; push ./results/`)
2. Enables efficient development from low-speed or high-jitter internet connections
3. Enables ssh-ing from remote->local even if local doesn't have a public IP (say when working behind public/shared internet)


# Setup

Perform the below steps on both the local and remote machines (replace all `<var>` with custom strings):

1. Add to `~/.ssh/config`,
	```
	### default for all ##
	Host *
	     ForwardAgent no
	     ForwardX11 no
	     ForwardX11Trusted no
	     Port 22
	     Protocol 2
	     ServerAliveInterval 60
	     ServerAliveCountMax 30
	```
	If you need to use X forwarding for certain applications, then use the first few lines above as, (and make sure to install `xauth`; `brew install xquartz` on mac)
	```
	     ForwardAgent yes
	     ForwardX11 yes
	     ForwardX11Trusted yes
	     XAuthLocation /opt/X11/bin/xauth
     ```

2. Copy binaries to `~/bin`,
	```
	cd remote_dev/
	bash setup.sh
	```
	The last step within `setup.sh` above assumes there is a `~/.bashrc` present, and `$PATH` available in the environment, so they are updated with the new binaries.
	If `~/.bashrc` is present, refresh current shell with the new binaries,
	```
	source ~/.bashrc
	```
	If `~/.bashrc` is not present, please add `~/bin` to `$PATH` by running,
	```
	export PATH=$PATH:~/bin
	```

3. Add the local's username to `~/bin/localuser.t` (even when following these steps to set up the remote):
	```
	echo "<localuser>" > ~/bin/localuser.t
	```

4. Add the local's home's string to `~/bin/localhomestr.t` (even when following these steps to set up the remote):
	```
	echo "home" > ~/bin/localhomestr.t
	```
	If the local is running linux, the home's string would be `home` and for osx, this would be `Users`


After performing above steps on both local as well as remote, password-less ssh-ing needs to be set up (preferably setting up local<-->remote bi-directionally for simplicity; but only remote->local is required for push/pull executed from remote to work, which is the usecase presented here).

For local->remote, run the command below, which will ask to type the ssh password twice:
```
bash setup_ssh.sh <remoteuser>@<remoteip>
```
For remote->local, one can either do the steps detailed by commands within `setup_ssh.sh` manually, or another way would be to first create an ssh tunnel from local->remote (by following step 1 in below section, "working remotely"), followed by ssh-ing on another terminal session to remote and running the command below:
```
bash setup_ssh.sh <localuser>@localhost
```


# Starting a remote session

To start working remotely with ssh tunnels, 
1. First create a tunnel local->remote by `tunnel <remoteuser>@<remoteip>` on local. Once the tunnel is established, leave that terminal session as is
2. Using a new terminal session, ssh into the remote, and from any folder on the remote, `pull` and `push` commands can be used, and the "corresponding folder" (explained below) on the local will be respectively pulled from or pushed to

Corresponding folders are `~/relative/folder/path/` matched on both machines, i.e., `remote:~/project/` will be pulled from `local:~/project/`. The specific folder to pull can be provided as a relative path through the first argument, for instance some usage examples are,
```
pull project_dir
pull ../dir/
pull fname.xyz
pull rel/path/to/folder/or/file
pull
```
the last of which, `pull`, corresponds to pulling the current working directory. Absolute paths are not supported as of now.

Notes:
- `tunnel` by default uses the port `8044` which can be changed by `echo <port> > ~/bin/port.t`
	- Multiple users logged into the same remote would have to use separate ports using the above
- `sshd` needs to be running on local for reverse ssh to work
	- If local is running osx, this neeeds to be enabled by checking "Remote Login" within System Preferences -> Sharing
- If using cloud computing infrastructure for remote, make sure that above port is exposed on their security console


# Glossary

An overview of the ssh tunneling based scripts introduced:
```
tunnel 		- create an ssh tunnel local->remote (run on the local)
pull 		- pull current/specified directory from local->remote (run on the remote)
pulldiff 	- see changes without actually pulling (uses rsync --dry-run, run on the remote)
push 		- push current/specified directory from remote->local (run on the remote)
killtunnels	- kill all ssh tunnels on remote if any is unresponsive (happens rarely, 
			run on the remote, needs to be a sudoer)
```
